from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import LocationVO, Hat
from .acls import get_photo


#! Need api_show_hat(request, id) @require_http_methods(["GET", "DELETE", "PUT"]) (detail of hat)
#! Need api_list_hats(request, location_vo_id=None) @require_http_methods(["GET", "POST"]) (list of hat)


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href",
    ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "location",
        "picture_url",
        "id",
    ]
    encoders = {
        "location": LocationVODetailEncoder()
    }

class HatDetailEncoder(ModelEncoder):   #! Dupliate as above, different name. Will
    model = Hat                         #! consolidate if dont need both
    properties = [
        "fabric",
        "style_name",
        "color",
        "location", #LocationVO object (2)
        "picture_url",
        "id"
    ]
    encoders = {                   #! PLURAL!!!
        "location": LocationVODetailEncoder()
    }



#!Hat Detail
@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_hat(request, id):
    if request.method == "GET":
        hat = Hat.objects.get(id=id)

        return JsonResponse(
            hat,
            HatDetailEncoder,
            False,
        )

    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=id).delete()

        return JsonResponse(
            {"delete: ": count>0}
        )

    else: #request.method == "PUT"
        content = json.loads(request.body)

        Hat.objects.filter(id=id).update(**content)
        hat = Hat.objects.get(id=id)

        return JsonResponse(
            hat,
            HatDetailEncoder,
            False
        )

#!Hat List
@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id = None):

    if request.method == "GET":

        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()

        return JsonResponse(
            {"hats": hats},
            HatListEncoder,
            False
        )

    else: #request.method=="POST"
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href= location_href)

            content["location"] = location

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message: " "Invalid location ID"},
                status=400
            )

        # photo = get_photo(content["fabric"], content["style_name"], content["color"])
        # content.update(photo)
        hat = Hat.objects.create(**content)

        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
