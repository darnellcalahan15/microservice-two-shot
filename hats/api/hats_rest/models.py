from django.db import models
from django.urls import reverse

# Create your models here.

#! Hats_rest is a "attendees" with in microservice called hats


#! It needs location so we need to create Location VO.
#! We need to create a class hats
#! Inside the calss we need picture_url



class LocationVO(models.Model):
    closet_name = models.CharField(max_length=200)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return f"{self.closet_name} - {self.section_number}/{self.shelf_number}"

class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"id": self.id})
