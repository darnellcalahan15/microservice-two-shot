from django.contrib import admin
from .models import Bin

from .models import Location

@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    pass
admin.site.register(Bin)
