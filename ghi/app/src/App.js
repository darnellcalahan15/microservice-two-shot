import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import HatForm from './HatForm';
import DisplayShoes from './ShoeList';
import ShoeForm from './ShoeForm';
import { useState, useEffect } from 'react';
import ShoeDetail from './ShoeDetail';
import AddItem from './AddItem';

function App() {

  const [hats, setHats] = useState([]);
  const [shoes, setShoes] = useState([]);
  const [lastShoe, setLastShoe] = useState(0);

  const shoeData= async () => {
    const response = await fetch('http://localhost:8080/api/shoes/');
    if (response.ok) {
      const data = await response.json();
      setShoes(data.res)

    }
  }

  async function getHats () {
    const hatsUrl = "http://localhost:8090/api/hats/";
    const response = await fetch(hatsUrl);

    if (response.ok) {
      const { hats } = await response.json();
      setHats(hats);
    } else {
      console.error("Error LEWIS!!!!!!!!!!!!!!!!!!!!");
    }
  };

  useEffect(() => {
    getHats();
    shoeData();
  }, []);

  if (hats === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatList hats={hats} getHats={getHats}/>} />
            <Route path="new" element={<HatForm getHats={getHats}/>} />
          </Route>
          <Route path='shoes'>
            <Route index element={<DisplayShoes shoes={shoes}/>} />
            <Route path='new' element={<ShoeForm newShoe={lastShoe} refresh= {shoeData} />} />
            <Route path= ':id' element={<ShoeDetail refresh={shoeData} shoes={shoes}/>} />
          </Route>
          <Route  path='additem' element={<AddItem />} />
        </Routes>
      </div>
    </BrowserRouter>
  );

}

export default App;
