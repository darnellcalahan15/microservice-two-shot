import React from "react";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";


const ShoeForm = ({refresh, newShoe}) => {
  const navigate = useNavigate();
  const [bins, setBins] = useState([]);
  const [filled, setFilled] = useState(false);

    const[data, setData] =useState({

      model_name:"",
      manufacturer: "",
      color: "",
      picture_url: "",
      // picture_file: "",
      bin: ""
    });


      const binData = async () => {
        const dataUrl = await fetch('http://localhost:8100/api/bins/');
        const data = await dataUrl.json()
        setBins(data.bins)
    }
    useEffect(()=>{
        binData()
    },[]);

    const handleChangeName = (event) => {
      const type = event.target.type;
      const name = event.target.name;
      const value = event.target.value
      setData(curData => ({
          ...curData,
          [name] : value
      }));
    }

     const postData = async (e)=> {
      e.preventDefault();


      const url = `http://localhost:8080/api/shoes/`

      const options = {
        method: 'post',
        headers: {
          'content-type': 'application/json'
        },
        body: JSON.stringify(data)
      };

      const response = await fetch(url, options);

      if (response.ok) {
        refresh();
        navigate(`/shoes`);
        // setFilled(true)
    }else{
      console.log("Error posting")
    }
    }

return (
  <div className="container mt-5">
  <h1 className="text-center mb-4">Add Shoe </h1>
    <form onSubmit={postData} className="">
  <div className="form-group mb-2">
    <input onChange={handleChangeName} type="text"  name="model_name" className="form-control" id="shoeModel" aria-describedby="shoeModel" placeholder=" Shoe Model" />
  </div>

  <div className="form-group mb-2">
    <input onChange={handleChangeName} type="text"  name="color" className="form-control" id="shoeColor" aria-describedby="shoeColor" placeholder="Shoe Color" />
  </div>

  <div className="form-group mb-2">
    <input onChange={handleChangeName} type="text"  name="manufacturer" className="form-control" id="shoeManu" aria-describedby="emailHelp" placeholder="Manufacturer" />
  </div>

  <div className="form-group mb-2">
    <input onChange={handleChangeName} type="url"  name="picture_url" className="form-control" id="pictureUrl" aria-describedby="urlofpicture" placeholder="picture url" />
  </div>

  <div className="dropdown mb-2 ">
  <label className="m-2"> </label>
  <select className="container" onChange={handleChangeName} name="bin">
    <option value="" hidden>Choose a bin</option>
  {bins.map(bin=> {return(
     <option key={bin.href}>{bin.closet_name}</option>
  )})}
    </select>
   </div>
  <button  type="submit" className="btn mt-2 btn-primary ">Submit</button>
</form>

</div>

)
}

export default ShoeForm
