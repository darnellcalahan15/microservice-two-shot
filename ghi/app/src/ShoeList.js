import React from "react";
import { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import ShoeDetail from "./ShoeDetail";

function DisplayShoes(props) {

  const tableHeaders = [
    "",
    'Model Name',
    'manufacturer',
    'Color',
    'Bin',

  ]
    return(

        <div>
            <table className="table table-striped">
  <thead className="thead-dark">
  <tr>
    {tableHeaders.map((headers, index)=>{
      return(
      <th key={index} scope="col">{headers}</th>
      )
    })}
      </tr>

  </thead>
  <tbody>
  {props.shoes.map((shoe,index)=>{return (
  <tr key={shoe.id}>
  <th scope="row">{index+1}</th>
        <td > <NavLink className="nav-link" to={`${shoe.id}`} >{shoe.name}</NavLink></td>
        <td>{shoe.manufacturer}</td>
        <td>{shoe.color}</td>
        <td>{shoe.bin.closet_name}</td>
        </tr>
    )})}
  </tbody>
</table>
        </div>
    )

}

export default DisplayShoes
