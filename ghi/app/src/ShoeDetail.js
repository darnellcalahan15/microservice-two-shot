import React, { useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";


const ShoeDetail = ({shoes, refresh})=> {
    const navigate = useNavigate();
   const itemId= useParams()
    const item =shoes.find(shoe => shoe.id === Number(itemId.id))

    const deleteShoe = async (e, id)=> {
        const I = itemId.id

        const url = `http://localhost:8080/api/shoe/${I}/`

        const options = {
          method: 'delete',
          headers: {
            'content-type': 'application/json'
          },
        };

        const response = await fetch(url, options);

        if (response.ok) {
            refresh();
            navigate("/shoes");
        }
    }

    return(

        <div className=" container card mt-5" style={{width: 250 }}>
  <img className="card-img-top" src={item.picture} alt="Card image cap" />
  <div className=" text-center card-body">
    <h4 className="card-title">{item.name}</h4>
    <h6 className="text-muted">by</h6>
    <p className="card-text">{item.manufacturer}</p>
    <button onClick={()=>deleteShoe(item.id)} type="click" className="btn btn-primary">Delete</button>
  </div>
</div>
    )
}

export default ShoeDetail
