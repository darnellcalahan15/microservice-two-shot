
import React, { useEffect, useState } from "react";

function HatForm ({getHats}) {

    const [locations, setLocations] = useState([]);

    const fetchData = async () => {
        const locationsUrl = "http://localhost:8100/api/locations/";

        const response = await fetch(locationsUrl);

        if (response.ok) {
            const {locations} = await response.json();
            setLocations(locations);
        }
    };

    const [styleName, setStyleName] = useState("");
    const [fabric, setFabric] = useState("");
    const [color, setColor] = useState("");
    const [pictureUrl, setPictureUrl] = useState("");
    const [location, setLocation] = useState("");

    const handleStyleNameChage = (event) => {
        const value = event.target.value;
        setStyleName(value);
    };

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    };

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    };

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    };

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value)
    };

    // !handleSubmit
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.style_name = styleName;
        data.fabric = fabric;
        data.color = color;
        data.picture_url = pictureUrl;
        data.location = location;



        const hatUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method:"post",
            body: JSON.stringify(data),
            header: {
                "Content-Type":"application/json"
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();

            setStyleName("")
            setFabric("");
            setColor("");
            setPictureUrl("");
            setLocation("");

            getHats();

          }

    } //!End of handleSubmit


    useEffect(() => {
        fetchData()
    }, []);

    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a Hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input value={styleName} onChange={handleStyleNameChage} placeholder="Style Name" required type="text" name="styleName" id="styleName" className="form-control"/>
                <label htmlFor="Style Name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                <label htmlFor="Fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="Color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={pictureUrl} onChange={handlePictureUrlChange} placeholder="Picture URL" required type="url" name="pictureUrl" id="pictureUrl" className="form-control"/>
                <label htmlFor="Picture URL">Picture URL</label>
              </div>
              <div className="mb-3">
                <select value={location} onChange={handleLocationChange} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                      <option key={location.href} value={location.href}>
                        {location.closet_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )


}

export default HatForm;
