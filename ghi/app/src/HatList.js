import React from "react";

function HatList({ hats, getHats }) {
  const deleteHat = async (id) => {
    const deleteUrl = `http://localhost:8090/api/hats/${id}/`;

    const deleteConfig = {
      method: "delete",
      header: {
        "Content-Type": "application.json",
      },
    };

    const response = await fetch(deleteUrl, deleteConfig);
    getHats();
  };

  return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Delete</th>
            <th>Fabric</th>
            <th>Style Name</th>
            <th>Color</th>
            <th>Location</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {hats.map((hat) => {
            return (
              <tr key={hat.href}>
                <td>
                  <button onClick={() => deleteHat(hat.id)}>Delete</button>
                </td>
                <td>{hat.fabric}</td>
                <td>{hat.style_name}</td>
                <td>{hat.color}</td>
                <td>
                  {`${hat.location.closet_name} - ${hat.location.section_number}/${hat.location.shelf_number}`}
                </td>
                <td>
                  {hat.picture_url}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
  );
}
export default HatList;
