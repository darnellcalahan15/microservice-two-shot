import { NavLink } from "react-router-dom"


const AddItem =() => {
  const items = [
    {
      'item':'shoe',
      'image': 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrqCEnpcj99PLKm9_purvm8nTUOoq2WAoF5Q&usqp=CAU'

  },
    {
      'item':'hat',
      'image':'https://img.ltwebstatic.com/images3_spmp/2023/10/27/c1/1698377219228a2ba3f8f9b77658c2b463d8e36216_thumbnail_600x.webp'

    }
  ]








  return(
    <div className="row p-3 m-3">
  {items.map((i, index) =>{ return(

    <div className="" style={{width: 250 }} key={index} >

    <div className="card-body text-center">
      <NavLink  to={`/${i.item}s/new`}> {`Add ${i.item}s`}
      <img className="card-img-top" src={i.image} alt="Card image cap" />

      </NavLink>
    </div>
   </div>
  )})}
</div>
  )


}

export default AddItem
