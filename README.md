# Wardrobify

Team:

* Lewis - Hats
* Darnell - Shoes

## Design

## Shoes microservice

 I spent a GREAT deal of time reseacthing and experimenting but ultimately decided to scrap all extras and instead opt for a simple and bare minimum approach. Creating only two models, the shoe and bin vo. Those models have only the required fields asked for in the instructions. While there are three different field types seen between the two models. Only the charfield and urlfield are being used. I created a filefield in an attempt to accept image uploads from a persons computer but abandoned that idea for more interesting things. Although I fully understand how it works now, Thanks largely to my partner, who has been increadibly helpful, I did not use the cusome encoder to handle django requests because I could not for the life of me understand one of the lines and did not want to use code I did not uderstand since previous experience has shown me that debugging when your app uses code that you copied and pasted with no understanding of how it works is a total nightmare (for me). I took the same approach for the react components.
## Hats microservice

Models: Created LocationVO and Hat. Using poller, LocationVO will grab Location objects. The Location model is in Wardrobe API. The unique key I used was href. I believe i could've used id field, but wanted to use href as a test. Besides import_href, we gets created using location.href in poller.py, for LocationVO model, I'm also creating fields = closet_name, section_number and shelf_number. All three are used to get the full location of where the hat is. Hat model has fabric, style_name, color, picture_url and location, simple. Location field is Foreign Key field using LocationVO model. 

With poller set up, which grabs data using port 8000, I'm able to retrieve data using port 8100. The data i'm referring to are locations. POST/GET location info through port 8100, Hats microserver gets and loads location data thorugh port 8000.  All PUT/POST/GET/DELETE for hats occur in port 8090. 
