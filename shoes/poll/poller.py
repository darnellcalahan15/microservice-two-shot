import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from shoes_rest, here.
# from shoes_rest.models import Something
from shoes_rest.models import Bin_vo

# def get_bin():
#     response = requests.get('http://wardrobe-api:8000/api/bins/')
#     content = json.loads(response.content)
#     for i in content['bins']:
#         Bin_vo.objects.update_or_create(
#             import_href=i['href'],
#             defaults={'closet_name':i[]}
#         )

def poll():
    while True:
        print('Shoes poller polling for PPPP')
        try:
            pass
            request=requests.get('http://wardrobe-api:8000/api/bins/')
            data = json.loads(request.content)
            for i in data['bins']:
                Bin_vo.objects.update_or_create(
                    import_href=i['href'],
                    defaults={'closet_name':i['closet_name']}
                )

            # Write your polling logic, here
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
