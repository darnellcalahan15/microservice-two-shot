from django.db import models
from django.urls import reverse
# Create your models here.

class Bin_vo(models.Model):
    closet_name = models.CharField(max_length=150)
    import_href = models.URLField( max_length=200, unique=True)



class ShoeModel(models.Model):
    model_name = models.CharField(max_length=200, blank=True)
    manufacturer = models.CharField(max_length=200, blank=True)
    color = models.CharField(max_length=100, blank=True)
    picture_url = models.URLField(blank=True)
    picture_file = models.FileField(null=True, blank=True)

    bin = models.ForeignKey(
        Bin_vo,
        related_name='shoes',
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )
    def get_api_url(self):
        return reverse('shoelist_view', kwargs={'pk': self.pk})

    def __str__(self):
        return f'{self.model_name}\n by: {self.manufacturer}'
