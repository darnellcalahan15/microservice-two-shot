# Generated by Django 4.0.3 on 2023-12-14 02:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0003_alter_shoemodel_bin'),
    ]

    operations = [
        migrations.AddField(
            model_name='bin_vo',
            name='import_href',
            field=models.URLField(default='eh', unique=True),
            preserve_default=False,
        ),
    ]
