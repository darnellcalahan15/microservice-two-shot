from django.urls import path
from .views import shoelist_view,delete_shoe

urlpatterns = [
    path('shoe/<int:pk>/', delete_shoe, name='delete_shoe'),
    path('shoes/', shoelist_view, name='shoelist_view')
]
