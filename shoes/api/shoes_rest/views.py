from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import ShoeModel, Bin_vo
from django.http import JsonResponse
import json


# Create your views here.


def delete_shoe(request, pk):
    if request.method == "DELETE":
        content = ShoeModel.objects.get(pk=pk)
        content.delete()

        return JsonResponse({'true': 'True'})


@require_http_methods(['GET','POST'])
def shoelist_view(request):
    if request.method == 'GET':
        content = ShoeModel.objects.all()
        d=[]
        for i in content:
            res = {}
            res['name']=i.model_name
            res['manufacturer']=i.manufacturer
            res['color']=i.color
            res['id']=i.pk
            res['picture']=i.picture_url
            res['bin']={
                'closet_name':i.bin.closet_name,
                'href':i.bin.import_href
            }
            d.append(res)

        return JsonResponse({'res':d})

    if request.method =='POST':
        content = json.loads(request.body)
        try:
            bin=content['bin']
            t=Bin_vo.objects.get(closet_name=bin)
            content['bin']=t
        except Bin_vo.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Invalid bin href"},
                status=400,
            )
        ShoeModel.objects.create(**content)
        print(content)


        return JsonResponse({'true':'True'})
